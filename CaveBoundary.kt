package com.example.cychen.alicerabbit

import android.graphics.RectF

/**
 * Created by cychen on 30/01/2018.
 */
class CaveBoundary {

    companion object {

        private var caveCenterXYScaled = arrayOf(4f,4f)

        fun setCaveEdgeLimit(cavePosition: Array<Int>) {
            this.caveCenterXYScaled[0] = cavePosition[0] + 0.5f
            this.caveCenterXYScaled[1] = cavePosition[1] + 0.5f
        }

        fun caveBox(): RectF{
            val caveCollisionBoxWidthScaled = 2f
            return RectF(
                    caveCenterXYScaled[0] - 0.5f * caveCollisionBoxWidthScaled,
                    caveCenterXYScaled[1] - 0.5f * caveCollisionBoxWidthScaled,
                    caveCenterXYScaled[0] + 0.5f * caveCollisionBoxWidthScaled,
                    caveCenterXYScaled[1] + 0.5f * caveCollisionBoxWidthScaled
            )
        }

    }
}