package com.example.cychen.alicerabbit

import android.content.Context
import android.media.AudioManager
import android.media.MediaDataSource
import android.media.MediaPlayer
import android.media.SoundPool
import android.util.Log
import java.io.IOException

/**
 * Created by cychen on 11/03/2018.
 */
class BGMusic {

    companion object {

        fun bgMP(context: Context): MediaPlayer {
            val bgMediaPlayer = MediaPlayer.create(context, R.raw.dreams)
            val mpErrorListener = mpOnErrorListener(context)
            bgMediaPlayer.setOnErrorListener(mpErrorListener)
            val mpCompletionListener = mpOnCompletionListener(context)
            bgMediaPlayer.setOnCompletionListener (mpCompletionListener )
            bgMediaPlayer.isLooping
            bgMediaPlayer.setVolume(0.2f,0.2f)
            return bgMediaPlayer
        }

        fun soundP(context: Context): SoundPool {
            val sp = SoundPool(10, AudioManager.STREAM_MUSIC,0)
            val soundID = arrayOf(0,1)

            soundID[0]= sp.load(context,R.raw.footstep,1)
            soundID[1]=sp.load(context, R.raw.slidingbox,1)

            return sp
        }

        fun mpOnCompletionListener(context: Context):MediaPlayer.OnCompletionListener{
            val mpCompletionListener = object: MediaPlayer.OnCompletionListener {
                override fun onCompletion(mp: MediaPlayer) {
                    if (mp != null) {
                        mp.stop()
                        mp.release()
                    }
                }
            }
            return mpCompletionListener
        }

        fun mpOnErrorListener(context: Context): MediaPlayer.OnErrorListener {
            val mpErrorListener = object : MediaPlayer.OnErrorListener {
                override fun onError(mp: MediaPlayer, what: Int, extra: Int): Boolean {
                    reStartMP(mp,context)
                    return true
                }
            }
            return mpErrorListener
        }

        fun reStartMP(mp:MediaPlayer,context: Context){
            mp.reset()
            try {
                mp.setDataSource(context.resources.openRawResourceFd(R.raw.dreams).fileDescriptor)
                mp.prepare()
            } catch (ioex:IOException) {
                Log.d("gameview", "$ioex")
                mp.stop()
                mp.release()
            }
        }

    }
}