package com.example.cychen.alicerabbit

/**
 * Created by cychen on 04/03/2018.
 */
class GameStatus {
    companion object {
        private var gameAlive = true

        fun updateGameAlive(alive:String){
            gameAlive = alive == "true"
        }

        fun gameAlive():Boolean{
            return gameAlive
        }
    }
}