package com.example.cychen.alicerabbit

import android.content.Context
import android.graphics.*
import android.media.SoundPool
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.MotionEvent
import android.view.View

/**
 * Created by cychen on 05/01/2018.
 */
class GameView(ctx: Context, val levelIndex: Int, val sp:SoundPool) : View(ctx) {

    val initCellWidth = 60
    var num: NumSetting = NumSetting(13, 20, 9, initCellWidth)

    var contextOfSLA: Context = ctx

    var selectedLevel: Array<String> = SelectedLevelActivity().selectedLevel(levelIndex)

    var cavePosition: Array<Int> = arrayOf(0, 0)

    var stringToBitmapMap: MutableMap<String, Bitmap>
    var stringToRectMap: MutableMap<String, Rect>
    var stringToMagicEddyWindRectMap: MutableMap<String, Rect>
    var magicBitmap: Bitmap

    private var eddyWindCount: Int = 1
    var charOfBoxOnGoal:String = "$"


    init {

        MoveResult.updateSelectedLevel(selectedLevel)
        cavePosition = CavePosition.locateCavePosition(num,selectedLevel)
        CaveBoundary.setCaveEdgeLimit(cavePosition)

        stringToRectMap = GameBitmap(contextOfSLA.resources).srcStringRectMap()
        stringToBitmapMap = GameBitmap(contextOfSLA.resources).srcStringBitmapMap()
        stringToMagicEddyWindRectMap = GameBitmap(contextOfSLA.resources).srcStringMagicEddyWindRectMap()
        magicBitmap = GameBitmap(contextOfSLA.resources).fullMagicBitmap

        Invader.little("wolf1","&","physical",arrayOf(12.5f,0.5f),cavePosition)
        Invader.little("wolf1","&","physical",arrayOf(12.5f,8.5f),cavePosition)
        Invader.little("wolf1","&","physical",arrayOf(0.5f,0.5f),cavePosition)
        Invader.little("wolf1","&","physical",arrayOf(12.5f,4.5f),cavePosition)
        Invader.little("wolf2","&","physical",arrayOf(0.5f,4.5f),cavePosition)
        Invader.little("wolf3","&","physical",arrayOf(6.5f,0.5f),cavePosition)
        Invader.little("wolf4","&","physical",arrayOf(6.5f,8.5f),cavePosition)

    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val width: Int = w / num.numOfCellsPerRow
        num.cellWidth = width
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        DrawGameBoard(contextOfSLA, canvas, num).apply {
            drawBackGround()
            //  drawGridLine()
            drawBoard()
        }
        if (!GameStatus.gameAlive()){
            drawGameOver(canvas)
        } else {
            drawInvader(canvas)
            drawMagic(canvas)
        }
    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!GameStatus.gameAlive()) {
            return true
        } else {
            if (event.action != MotionEvent.ACTION_DOWN) {
                return true
            }
            charOfBoxOnGoal = TouchAndMove(event, num, sp).result()
            when (charOfBoxOnGoal) {
                "R" -> Magic.blackHole()
                "O" -> Magic.wave("R", cavePosition)
                "B" -> Magic.eddyWind("UR", cavePosition)
                "G" -> Magic.snow()
            }
            postInvalidate()
            return true
        }
    }

    fun isGameOver():Boolean {
        return (InvaderCollection.invaderList.isEmpty())
    }


    fun drawGameOver(canvas: Canvas){
        canvas.drawText("You Win", 3f * num.cellWidth, 5f * num.cellWidth, paint("text"))
    }

    fun drawCaveBox(canvas: Canvas){
        val box = CaveBoundary.caveBox()
        val length = num.cellWidth.toFloat()
        canvas.drawRect(box.left * length, box.top * length,
                box.right * length, box.bottom *length,
                paint("blackHole"))
    }

    fun drawMagic(canvas: Canvas){
        val newMagicList = mutableListOf<Magic>()
        if (eddyWindCount > 4) {
            eddyWindCount = 1
        }
        for (magic in MagicCollection.magicList) {
            val magicNew = MoveMagic.move(magic,num)
            if (magicNew.lifeStatus){
                newMagicList.add(magicNew)
                drawMagicImage(canvas,magic)
            }
        }
        eddyWindCount += 1
        MagicCollection.updateMagicList(newMagicList)
    }

    fun drawMagicImage(canvas: Canvas,magic: Magic){
        when (magic.magicType) {
            "blackHole" -> {
                canvas.drawCircle(magic.centerXY[0], magic.centerXY[1], magic.radius,
                        paint(magic.magicType)) }
            "wave" -> {
                canvas.drawArc(magic.imageRectF, magic.waveStartAngle, magic.waveSweepAngle,
                        false, paint(magic.magicType)) }
            "eddyWind" -> {
                val sym = "WIND$eddyWindCount"
                canvas.drawBitmap(magicBitmap, stringToMagicEddyWindRectMap[sym], magic.imageRectF,
                        null) }
            "snow" -> {
                magic.centerXYList.forEach {
                canvas.drawCircle(it[0], it[1], magic.radius, paint(magic.magicType)) } }
        }
    }

    fun drawInvader(canvas: Canvas) {
        val survivedInvaderList = mutableListOf<Invader>()
        for (invader in InvaderCollection.invaderList) {
            val invaderNew = MoveInvader.move(invader,num)
            if (invaderNew.lifeStatus) {
                survivedInvaderList.add(invader)
                val label = invaderNew.label
                canvas.drawBitmap(stringToBitmapMap[label], stringToRectMap[label],
                    invaderNew.imageRectF, null)
            }
        }
        InvaderCollection.updateInvaderList(survivedInvaderList)
    }


    fun paint(magicType:String):Paint {
        var brush = Paint()
        val snowColor: Int = ContextCompat.getColor(contextOfSLA, R.color.gameviewBaseColor)
        val blackHoleColor :Int = ContextCompat.getColor(contextOfSLA, R.color.colorPrimaryDark)
        val waveColor:Int = ContextCompat.getColor(contextOfSLA, R.color.gameviewArcColor)
        val textColor:Int = ContextCompat.getColor(contextOfSLA,R.color.gameOverTextColor)
        val snowBrush = Paint()
        val blackHoleBrush = Paint()
        val waveBrush = Paint()
        val textBrush = Paint()
        snowBrush.color = snowColor
        blackHoleBrush.color = blackHoleColor
      //  blackHoleBrush.style = Paint.Style.STROKE
        waveBrush.color = waveColor
        waveBrush.strokeWidth = 20f
        waveBrush.style = Paint.Style.STROKE
        textBrush.color = textColor
        textBrush.textSize = 166f
        textBrush.strokeWidth = 5f
        textBrush.textSkewX = -0.5f

        when (magicType){
            "snow" -> brush = snowBrush
            "blackHole" -> brush = blackHoleBrush
            "wave" -> brush = waveBrush
            "text" -> brush = textBrush
        }
        return brush
    }
}