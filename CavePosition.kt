package com.example.cychen.alicerabbit

/**
 * Created by cychen on 08/02/2018.
 */
class CavePosition {
    companion object {
        fun locateCavePosition(num: NumSetting,selectedLevel:Array<String>) : Array<Int> {
            var cavePosition = arrayOf(0,0)
            val (_,_,numOfCellsPerColumnInBattleField,_)=num
            for (rIndex in 0 until numOfCellsPerColumnInBattleField){
                for ((cIndex,value) in selectedLevel[rIndex].withIndex()){
                    if (value == 'c'){
                        cavePosition[0] = cIndex
                        cavePosition[1] = rIndex
                        return cavePosition
                    }
                }
            }
            return cavePosition
        }
    }
}