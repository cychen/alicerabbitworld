package com.example.cychen.alicerabbit

import android.util.Log

/**
 * Created by cychen on 05/03/2018.
 */

class ARWorldEventListener {

    var mListener: GameOverEventListener ?= null

    fun setGameOverEventListener(ev:GameOverEventListener){
        mListener = ev
    }

    fun gameOverDetected(){
        if (mListener != null){
            mListener!!.onGameOver()
        }
    }
}
