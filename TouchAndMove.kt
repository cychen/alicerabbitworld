package com.example.cychen.alicerabbit

import android.media.SoundPool
import android.util.Log
import android.view.MotionEvent

/**
 * Created by cychen on 19/01/2018.
 */
class TouchAndMove(event: MotionEvent, val num: NumSetting, val sp:SoundPool) {

    val touchX: Int
    val touchY: Int

    val newSelectedLevel: Array<String>

    var tCIndex: Int
    var tRIndex: Int

    val TC_MIN: Int
    val TC_MAX: Int
    val TR_MIN: Int
    val TR_MAX: Int

    var manRIndex: Int=0
    var manCIndex: Int=0
    var manTargetR: Int=0
    var manTargetC: Int=0
    
    var boxRIndex: Int = 0
    var boxCIndex: Int =0
    var boxTargetR: Int=0
    var boxTargetC: Int=0
    
    val manCharMap: MutableMap<Char, Char>
    val boxCharMap: MutableMap<Char, Char>
    val charAcceptMoveIn: ArrayList<Char>
    val charHasMan: ArrayList<Char>
    
    var charAtTop: Char = 'f'
    var charAtBottom: Char = 'f'
    var charAtRight: Char = 'f'
    var charAtLeft: Char = 'f'
    var charOfBoxOnGoal : String = "$"
    init {
        val (numOfCellsPerRow, numOfCellsPerColumn,numOfCellsPerColumnInBattleField,cellWidth) = num
        TC_MIN = 1
        TC_MAX = numOfCellsPerRow-2
        TR_MIN = numOfCellsPerColumnInBattleField+1
        TR_MAX = numOfCellsPerColumn - 2

        touchX = event.x.toInt()
        touchY = event.y.toInt()
        tCIndex = touchX / cellWidth
        tRIndex = touchY / cellWidth

        newSelectedLevel = MoveResult.selectedLevel

        manCharMap = TileCharMap().manCharMap()
        boxCharMap = TileCharMap().boxCharMap()
        charAcceptMoveIn = TileCharMap().charAcceptMoveIn
        charHasMan = TileCharMap().charHasMan

    }


    fun result():String {

        val charAtTouch = newSelectedLevel[tRIndex].elementAt(tCIndex)

        when {
            (tCIndex in TC_MIN..TC_MAX) && (tRIndex in TR_MIN..TR_MAX) -> {

                if (charAtTouch == '$') {
                    
                    charOfBoxOnGoal = isBoxCase()
                    
                } else if (charAcceptMoveIn.contains(charAtTouch)) {
                    
                    noBoxCase()
                }
                MoveResult.updateSelectedLevel(newSelectedLevel)
            }
            else -> MoveResult.updateSelectedLevel(newSelectedLevel)
        }
        return charOfBoxOnGoal
    }


    fun noBoxCase() {

        charAtTop = newSelectedLevel[tRIndex - 1].elementAt(tCIndex)
        charAtBottom = newSelectedLevel[tRIndex + 1].elementAt(tCIndex)
        charAtRight = newSelectedLevel[tRIndex].elementAt(tCIndex + 1)
        charAtLeft = newSelectedLevel[tRIndex].elementAt(tCIndex - 1)

        when {
            charHasMan.contains(charAtTop) ->
                handleMan('D')
            charHasMan.contains(charAtRight) ->
                handleMan('L')
            charHasMan.contains(charAtBottom) ->
                handleMan('U')
            charHasMan.contains(charAtLeft) ->
                handleMan('R')
        }
    }

    fun handleMan(direction: Char) {
        manTargetR=tRIndex
        manTargetC=tCIndex
        when (direction){
            'D' -> {
                manRIndex=tRIndex-1
                manCIndex=tCIndex
            }
            'U' -> {
                manRIndex=tRIndex+1
                manCIndex=tCIndex
            }
            'R' -> {
                manRIndex=tRIndex
                manCIndex=tCIndex-1
            }
            'L' -> {
                manRIndex=tRIndex
                manCIndex=tCIndex+1
            }
        }
        moveMan()

    }

    fun isBoxCase(): String {

        charAtTop = newSelectedLevel[tRIndex - 1].elementAt(tCIndex)
        charAtBottom = newSelectedLevel[tRIndex + 1].elementAt(tCIndex)
        charAtRight = newSelectedLevel[tRIndex].elementAt(tCIndex + 1)
        charAtLeft = newSelectedLevel[tRIndex].elementAt(tCIndex - 1)

        when {
            charHasMan.contains(charAtTop) && charAcceptMoveIn.contains(charAtBottom)
            -> {
               charOfBoxOnGoal = handleBox('D')
                handleMan('D')
            }
            charHasMan.contains(charAtRight) && charAcceptMoveIn.contains(charAtLeft)
            -> {
                charOfBoxOnGoal = handleBox('L')
                handleMan('L')
            }
            charHasMan.contains(charAtBottom) && charAcceptMoveIn.contains(charAtTop)
            -> {
                charOfBoxOnGoal = handleBox('U')
                handleMan('U')
            }
            charHasMan.contains(charAtLeft) && charAcceptMoveIn.contains(charAtRight)
            -> {
                charOfBoxOnGoal = handleBox('R')
                handleMan('R')
            }
        }
        return charOfBoxOnGoal
    }

    fun handleBox(direction: Char): String {
        boxRIndex = tRIndex
        boxCIndex = tCIndex
        when (direction){
            'D' -> {
                boxTargetR=tRIndex+1
                boxTargetC=tCIndex
            }
            'U' -> {
                boxTargetR=tRIndex-1
                boxTargetC=tCIndex
            }
            'R' -> {
                boxTargetR=tRIndex
                boxTargetC=tCIndex+1
            }
            'L' -> {
                boxTargetR=tRIndex
                boxTargetC=tCIndex-1
            }
        }
        return moveBox()

    }


    fun moveMan(){
        manOutCurrentTile()
        manInTargetTile()
        sp.play(1,1f,1f,0,0,1f)
    }

    fun moveBox(): String {
        boxOutCurrentTile()
        sp.play(2,1f,1f,0,0,1f)
        return boxInTargetTile()
    }

    fun manOutCurrentTile(){
        var currentString = newSelectedLevel[manRIndex].toCharArray()
        val charAtManPosition = currentString.elementAt(manCIndex)
        val remainedChar: Char = manCharMap.getOrDefault(charAtManPosition, 'f')
        currentString.set(manCIndex, remainedChar)
        newSelectedLevel[manRIndex]= String(currentString)
    }

    fun manInTargetTile() {
        var targetString = newSelectedLevel[manTargetR].toCharArray()
        val charAtManTargetTile = targetString.elementAt(index = manTargetC)
        val tileFinalChar: Char = manCharMap.getOrDefault(charAtManTargetTile, 'f')
        targetString.set(manTargetC, tileFinalChar)
        newSelectedLevel[manTargetR]= String(targetString)
    }

    fun boxOutCurrentTile() {
        var currentString = newSelectedLevel[boxRIndex].toCharArray()
        val charAtManTargetTile = currentString.elementAt(index = boxCIndex)
        val remainedChar: Char = boxCharMap.getOrDefault(charAtManTargetTile,'f')
        currentString.set(boxCIndex,remainedChar)
        newSelectedLevel[boxRIndex]= String(currentString)
    }

    fun boxInTargetTile(): String {
        var targetString = newSelectedLevel[boxTargetR].toCharArray()
        val charAtBoxTargetTile = targetString.elementAt(index = boxTargetC)
        val tileFinalChar: Char = boxCharMap.getOrDefault(charAtBoxTargetTile, 'f')
        targetString.set(boxTargetC, tileFinalChar)
        newSelectedLevel[boxTargetR] = String(targetString)
        return "${tileFinalChar}"
    }
    
}