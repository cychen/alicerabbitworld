package com.example.cychen.alicerabbit

import android.graphics.Rect
import android.graphics.RectF
import android.util.Log
import java.util.*

/**
 * Created by cychen on 22/01/2018.
 */
class Invader {
        var ID: Int = 1
        var name : String = "wolf"
        var label : String = "&"
        var attackType : String = "physical"
        val friend: Boolean = false
        var life: Int = 20
        var lifeRemained: Int = 20
        var attackDamagePoint: Int = 1
        var lifeStatus: Boolean = true
        var centerXYScaled: Array<Float> = arrayOf(11f,4f)
        var imageRectFScaled : RectF = RectF(11f,4f,12f,5f)
        var imageRectF: RectF = imageRectFScaled
        var collisionBoxScaled: RectF = imageRectFScaled
        var collisionBox: RectF = imageRectF
        var stepSizeScaled: Array<Float> = arrayOf(0.1f,0.1f)

        companion object {

            fun little(name: String, label: String, attackType:String, centerXYScaled: Array<Float>,cavePosition: Array<Int>) {

                val velocity = 0.001f
                val msTimeInterval: Long = 50
                val distance: Float = velocity * msTimeInterval

                val xVector = (cavePosition[0] + 0.5f) - centerXYScaled[0]
                val yVector = (cavePosition[1] + 0.5f) - centerXYScaled[1]

                val lengthVector = Math.sqrt((xVector * xVector + yVector * yVector).toDouble()).toFloat()

                val xStepSize = (distance * (xVector/lengthVector))
                val yStepSize = (distance * (yVector/lengthVector))

                val stepSizeScaled = arrayOf(xStepSize,yStepSize)

                val imageRectFWidthScaled = 1f
                val imageRectFScaled = RectF(
                        centerXYScaled[0]- 0.5f * imageRectFWidthScaled,
                        centerXYScaled[1] - 0.5f * imageRectFWidthScaled,
                        centerXYScaled[0] + 0.5f * imageRectFWidthScaled,
                        centerXYScaled[1] + 0.5f * imageRectFWidthScaled)

                val little = Invader()
                little.name = name
                little.label = label
                little.attackType = attackType
                little.stepSizeScaled = stepSizeScaled
                little.centerXYScaled = centerXYScaled
                little.imageRectFScaled = imageRectFScaled
                little.imageRectF = imageRectFScaled
                little.collisionBoxScaled = imageRectFScaled
                little.collisionBox = imageRectFScaled
                InvaderCollection.addInvader(little)
            }

            fun boss(name:String, label:String, attackType:String, centerXYScaled: Array<Float>, cavePosition: Array<Int>){
                val velocity = 0.001f
                val msTimeInterval: Long = 50
                val distance: Float = velocity * msTimeInterval

                val xVector = (cavePosition[0]+0.5f) - centerXYScaled[0]
                val yVector = (cavePosition[1]+0.5f) - centerXYScaled[1]

                val lengthVector = Math.sqrt((xVector * xVector + yVector * yVector).toDouble()).toFloat()

                val xStepSize = (distance * (xVector/lengthVector))
                val yStepSize = (distance * (yVector/lengthVector))

                val stepSizeScaled = arrayOf(xStepSize,yStepSize)

                val imageRectFWidthScaled = 1f
                val imageRectFScaled = RectF(
                        centerXYScaled[0] -  0.5f * imageRectFWidthScaled,
                        centerXYScaled[1] - 0.5f * imageRectFWidthScaled,
                        centerXYScaled[0] + 0.5f * imageRectFWidthScaled,
                        centerXYScaled[1] + 0.5f * imageRectFWidthScaled)

                val boss = Invader()
                boss.name = name
                boss.label = label
                boss.attackType = attackType
                boss.life = 10
                boss.lifeRemained = 10
                boss.attackDamagePoint = 3
                boss.stepSizeScaled = stepSizeScaled
                boss.centerXYScaled = centerXYScaled
                boss.imageRectFScaled = imageRectFScaled
                boss.imageRectF = imageRectFScaled
                boss.collisionBoxScaled = imageRectFScaled
                boss.collisionBox = imageRectFScaled
                InvaderCollection.addInvader(boss)
            }
        }


}








