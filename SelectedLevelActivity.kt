package com.example.cychen.alicerabbit

import android.content.Context
import android.content.res.Resources
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.example.cychen.alicerabbit.R.*

/**
 * Created by cychen on 05/01/2018.
 */

class SelectedLevelActivity: AppCompatActivity() {

    val tickerHandler = Handler()
    var timeTickerRunnable: Runnable? = null
    val aRWorldEventListener = ARWorldEventListener()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        this.window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        this.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)

        val sp = BGMusic.soundP(this)
        val bgMP = BGMusic.bgMP(this)
        bgMP.start()

        GameStatus.updateGameAlive("true")
        InvaderCollection.invaderList.clear()
        MagicCollection.magicList.clear()
        MoveResult.selectedLevel = emptyArray()

        val levelIndex = intent.getIntExtra("selectedLevelIndex", 9)

        val gameView = GameView(this@SelectedLevelActivity, levelIndex,sp)

        setContentView(gameView)

        val gameOverListener = gameOverEventListener(sp,bgMP)
        aRWorldEventListener.setGameOverEventListener(gameOverListener)

        timeTickerRunnable = timeTicker(gameView)
        tickerHandler.post(timeTickerRunnable)

    }


    fun selectedLevel(levelIndex: Int): Array<String> {
        val level: Array<String> = GameLevel().getLevel(levelIndex)
        return level
    }

    fun gameOverEventListener(sp:SoundPool,bgMP:MediaPlayer): GameOverEventListener {
        val ev = object : GameOverEventListener {
            override fun onGameOver() {
                finish()
                tickerHandler.removeCallbacks(timeTickerRunnable)
                sp.release()
                bgMP.stop()
                bgMP.release()
            }
        }
        return ev
    }

    fun timeTicker(gameView:GameView):Runnable {
        var startTime = System.currentTimeMillis()
        val fixedTimePeriod = 200L

        val ticker = object : Runnable {

            override fun run(){

                if (gameView.isGameOver()) {
                    GameStatus.updateGameAlive("false")
                    aRWorldEventListener.gameOverDetected()
                } else {
                    val now = System.currentTimeMillis()
                    val deltaTime = now - startTime
                    var quot = (deltaTime / fixedTimePeriod).toInt()
                    val timeLag = deltaTime % fixedTimePeriod
                    var msUpdateGameView = fixedTimePeriod - timeLag

                    startTime = now

                    when (quot) {
                        0 -> tickerHandler.postDelayed(this, fixedTimePeriod)
                        1 -> tickerHandler.postDelayed(this, msUpdateGameView)
                        else -> {
                            do {
                                gameView.postInvalidate()
                                quot -= 1
                            } while (quot > 1)
                            tickerHandler.postDelayed(this, msUpdateGameView)
                        }
                    }
                }
                gameView.postInvalidate()
            }
        }
        return ticker
    }

/*
    fun soundP():SoundPool {
        val sp = SoundPool(10,AudioManager.STREAM_MUSIC,0)
        val soundID = arrayOf(0,1)

        soundID[0]= sp.load(this@SelectedLevelActivity,R.raw.footstep,1)
        soundID[1]=sp.load(this@SelectedLevelActivity, raw.slidingbox,1)

        return sp
    }

*/
}