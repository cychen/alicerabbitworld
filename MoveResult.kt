package com.example.cychen.alicerabbit

/**
 * Created by cychen on 19/01/2018.
 */
class MoveResult {

    companion object {

        var selectedLevel : Array<String> = arrayOf()

        fun updateSelectedLevel( selectedLevel: Array<String>){
            this.selectedLevel = selectedLevel
        }
    }
}