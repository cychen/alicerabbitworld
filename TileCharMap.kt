package com.example.cychen.alicerabbit

/**
 * Created by cychen on 17/01/2018.
 */
class TileCharMap {

    val charAcceptMoveIn: ArrayList<Char>
    get() = arrayListOf(' ', '.', 'r', 'o', 'g', 'b')

    val charHasMan: ArrayList<Char>
    get() = arrayListOf('@','+','1','2','3','4')

    val srcManCharMap: MutableMap<Char, Char> = mutableMapOf<Char,Char>()
    val srcBoxCharMap: MutableMap<Char, Char> = mutableMapOf<Char,Char>()

    fun manCharMap(): MutableMap<Char, Char> {
        return setManCharMap()
    }

    fun boxCharMap(): MutableMap<Char, Char> {
        return setBoxCharMap()
    }


    fun setManCharMap(): MutableMap<Char, Char> {

        if (srcManCharMap.isEmpty()) {

            srcManCharMap['@'] = ' ' //man out, empty remained
            srcManCharMap['+'] = '.' // man out, goal remained

            srcManCharMap['1'] = 'r' // man out,RedPad
            srcManCharMap['2'] = 'o' // man out,OrangePad
            srcManCharMap['3'] = 'g' // man out,GreenPad
            srcManCharMap['4'] = 'b' // man out,BluePad

            srcManCharMap[' '] = '@' // empty, man in
            srcManCharMap['.'] = '+' // goal, man in

            srcManCharMap['r'] = '1' // RedPad, man in
            srcManCharMap['o'] = '2' // OrangePad, man in
            srcManCharMap['g'] = '3' // GreenPad, man in
            srcManCharMap['b'] = '4' // BluePad, man in

        }
        return srcManCharMap
    }


    fun setBoxCharMap(): MutableMap<Char, Char> {

        if (srcBoxCharMap.isEmpty()) {

            srcBoxCharMap['$'] = ' ' //box out, empty remained

            srcBoxCharMap[' '] = '$' // empty, box in
            srcBoxCharMap['.'] = '*' //goal, box in

            srcBoxCharMap['r'] = 'R' //RedPad, box in
            srcBoxCharMap['o'] = 'O' //OrangePad, box in
            srcBoxCharMap['g'] = 'G' //GreenPad, box in
            srcBoxCharMap['b'] = 'B' //BluePad, box in

        }
        return srcBoxCharMap
    }
}