package com.example.cychen.alicerabbit

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect

/**
 * Created by cychen on 07/01/2018.
 */
class GameBitmap(res: Resources){

    var fullARWorldBitmap: Bitmap
    var fullOgreBitmap: Bitmap

  //  var fullOceanBitmap: Bitmap
    var iconWidthInARWorld: Int = 0
    var iconWidthInOgre: Int = 0

    var fullMagicBitmap: Bitmap

    var iconWidthInMagic: Int = 0


    init {

        //val numOfIconsPerRowInARWorld = 8

        fullARWorldBitmap = BitmapFactory.decodeResource(res, R.drawable.arworld)
        fullOgreBitmap = BitmapFactory.decodeResource(res, R.drawable.ogre)

        //   fullOceanBitmap = BitmapFactory.decodeResource(res, R.drawable.ocean)

        iconWidthInARWorld = fullARWorldBitmap.width/8
        iconWidthInOgre = fullOgreBitmap.width / 5
        fullMagicBitmap = BitmapFactory.decodeResource(res, R.drawable.magic)

        iconWidthInMagic = fullMagicBitmap.width /4

    }

    private fun iconRect(left: Int, top: Int): Rect {
        val leftX = left * iconWidthInARWorld
        val topY = top * iconWidthInARWorld
        val rightX = (left+1) * iconWidthInARWorld
        val bottomY = (top + 1) * iconWidthInARWorld
        return Rect(leftX, topY, rightX, bottomY)
    }

    private fun iconRect2(left: Int, top: Int, iconWidth: Int): Rect {
        val leftX = left * iconWidth
        val topY = top * iconWidth
        val rightX = (left + 1) * iconWidth
        val bottomY = (top + 1)  * iconWidth
        return Rect(leftX, topY, rightX, bottomY)
    }


   private fun srcRectCollection(): MutableMap<String, Rect> {

       val rectCollection = mutableMapOf<String, Rect>()

       rectCollection["WALL_RECT"] = iconRect(2,13 )
       rectCollection["BG_RECT"] = iconRect(6,6)
       rectCollection["FIELD_RECT"] = iconRect(6,5 )
       rectCollection["MAN_RECT"] = iconRect2(3,2, iconWidthInOgre)
       rectCollection["BOX_RECT"] = iconRect(5,4 )
       rectCollection["FLAG_RECT"] = iconRect(5,5 )

       rectCollection["CAVE_RECT"] = iconRect(4,14)
       rectCollection["WOLF_RECT"] = iconRect2(0,6,iconWidthInOgre)

       rectCollection["RED_PAD_RECT"] = iconRect(0,0)
       rectCollection["ORANGE_PAD_RECT"] = iconRect(2,0)
       rectCollection["GREEN_PAD_RECT"] = iconRect(3,1)
       rectCollection["BLUE_PAD_RECT"] = iconRect(1,2)

       rectCollection["MAN_ON_GOAL_RECT"] = iconRect(4,3)
       rectCollection["MAN_ON_RED_RECT"] = iconRect(6,4)
       rectCollection["MAN_ON_ORANGE_RECT"] = iconRect(6,5)
       rectCollection["MAN_ON_GREEN_RECT"] = iconRect(6,5)
       rectCollection["MAN_ON_BLUE_RECT"] = iconRect(6,5)

       rectCollection["BOX_ON_GOAL_RECT"] = iconRect(4,4)
       rectCollection["BOX_ON_RED_RECT"] = iconRect(4,0)
       rectCollection["BOX_ON_ORANGE_RECT"] = iconRect(6,0)
       rectCollection["BOX_ON_GREEN_RECT"] = iconRect(6,1)
       rectCollection["BOX_ON_BLUE_RECT"] = iconRect(5,2)

       return rectCollection
   }

   private val bitmapList: ArrayList<Bitmap> = arrayListOf()

   private fun loadSrcBitmap(): ArrayList<Bitmap> {
       if (bitmapList.isEmpty()) {
           bitmapList.add(fullARWorldBitmap)
           bitmapList.add(fullOgreBitmap)
         //  bitmapList.add(redPadNo)
       }
       return bitmapList
   }


   fun srcBitmapList(): ArrayList<Bitmap> {
       return loadSrcBitmap()
   }


   private fun setStringToSrcRectMap(): MutableMap<String, Rect> {
       val stringToSrcRectMap = mutableMapOf<String, Rect>()
          for (entry in srcRectCollection()) {

              when (entry.key) {

                  "WALL_RECT" -> stringToSrcRectMap["#"] = entry.value
                  "BG_RECT" -> stringToSrcRectMap[" "] = entry.value
                  "FIELD_RECT" -> stringToSrcRectMap["f"] = entry.value
                  "MAN_RECT" -> stringToSrcRectMap["@"] = entry.value
                  "BOX_RECT" -> stringToSrcRectMap["$"] = entry.value
                  "FLAG_RECT" -> stringToSrcRectMap["."] = entry.value

                  "CAVE_RECT" -> stringToSrcRectMap["c"] = entry.value
                  "WOLF_RECT" -> stringToSrcRectMap["&"] = entry.value

                  "RED_PAD_RECT" -> stringToSrcRectMap["r"] = entry.value
                  "ORANGE_PAD_RECT" -> stringToSrcRectMap["o"] = entry.value
                  "GREEN_PAD_RECT" -> stringToSrcRectMap["g"] = entry.value
                  "BLUE_PAD_RECT" -> stringToSrcRectMap["b"] = entry.value

                  "MAN_ON_GOAL_RECT" -> stringToSrcRectMap["+"] = entry.value
                  "MAN_ON_RED_RECT" -> stringToSrcRectMap["1"] = entry.value
                  "MAN_ON_ORANGE_RECT" -> stringToSrcRectMap["2"] = entry.value
                  "MAN_ON_GREEN_RECT" -> stringToSrcRectMap["3"] = entry.value
                  "MAN_ON_BLUE_RECT" -> stringToSrcRectMap["4"] = entry.value

                  "BOX_ON_GOAL_RECT" -> stringToSrcRectMap["*"] = entry.value
                  "BOX_ON_RED_RECT" -> stringToSrcRectMap["R"] = entry.value
                  "BOX_ON_ORANGE_RECT" -> stringToSrcRectMap["O"] = entry.value
                  "BOX_ON_GREEN_RECT" -> stringToSrcRectMap["G"] = entry.value
                  "BOX_ON_BLUE_RECT" -> stringToSrcRectMap["B"] = entry.value

              }
          }
       return stringToSrcRectMap
   }


   fun srcStringRectMap(): MutableMap<String,Rect> {
       return setStringToSrcRectMap()
   }


   fun setStringToSrcBitmapMap(): MutableMap<String, Bitmap> {
       val stringToSrcBitmapMap = mutableMapOf<String, Bitmap>()
       for (entry in srcStringRectMap()) {
           when (entry.key) {
           "@" -> stringToSrcBitmapMap["@"] = srcBitmapList()[1]
           "&" -> stringToSrcBitmapMap["&"] = srcBitmapList()[1]
            else -> stringToSrcBitmapMap[entry.key] = srcBitmapList()[0]
           }
       }
       return stringToSrcBitmapMap
   }


   fun srcStringBitmapMap(): MutableMap<String, Bitmap> {
       return setStringToSrcBitmapMap()
   }


    fun magicEddyWindRectCollection(): MutableMap<String, Rect> {

        val rectCollection = mutableMapOf<String, Rect>()

    //    rectCollection["TORNADO_RECT"] = iconRect2(3, 2,iconWidthInMagic)
        rectCollection["WIND1_RECT"] = iconRect2(0, 5,iconWidthInMagic)
        rectCollection["WIND2_RECT"] = iconRect2(1, 5,iconWidthInMagic)
        rectCollection["WIND3_RECT"] = iconRect2(2, 5,iconWidthInMagic)
        rectCollection["WIND4_RECT"] = iconRect2(3, 5,iconWidthInMagic)
    //    rectCollection["FIRE_RECT"] = iconRect2(0, 6,iconWidthInMagic)

        return rectCollection
    }

    private fun setStringToMagicEddyWindRect(): MutableMap<String,Rect> {
        val stringToMagicEddyWindRect = mutableMapOf<String,Rect>()
        for (entry in magicEddyWindRectCollection()){
            when (entry.key){
              //  "TORNADO_RECT" -> stringToMagicRect['T'] = entry.value
                "WIND1_RECT" -> stringToMagicEddyWindRect["WIND1"]= entry.value
                "WIND2_RECT" -> stringToMagicEddyWindRect["WIND2"]= entry.value
                "WIND3_RECT" -> stringToMagicEddyWindRect["WIND3"]= entry.value
                "WIND4_RECT" -> stringToMagicEddyWindRect["WIND4"]= entry.value
              //  "FIRE_RECT" -> stringToMagicRect['F'] = entry.value
            }
        }
        return stringToMagicEddyWindRect
    }

    fun srcStringMagicEddyWindRectMap(): MutableMap<String, Rect> {
        return setStringToMagicEddyWindRect()
    }

}