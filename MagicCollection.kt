package com.example.cychen.alicerabbit

/**
 * Created by cychen on 01/02/2018.
 */
class MagicCollection {

    companion object {

        var magicID = 1
        var magicList = mutableListOf<Magic>()
        fun addMagic(newMagic:Magic){
            newMagic.ID = magicID
            magicList.add(newMagic)
            magicID += 1
        }

        fun updateMagicList(newMagicList: MutableList<Magic>){
            this.magicList = newMagicList
        }
    }

}