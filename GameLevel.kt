package com.example.cychen.alicerabbit

/**
 * Created by cychen on 03/01/2018.
 */

// from sokoban wiki: element vs char
// wall ( # )
// player ( @ )
// Box ( $ )
// Goal square ( . )   ManOnGoal ( + )      BoxOnGoal ( * )
// Floor ( space )
// bug ( & )
// cave ( c )
// power ( p )
// redPad ( r )  boxOnRedPad ( R/p ) manOnRedPad ( clean )
// orangePad ( o)  boxOnOrangePad ( O/p )   manOnOrangePad ( 2 )
// greenPad ( g )  boxOnGreenPad ( G/p )  manOnGreenPad ( 4 )
// bluePad ( b )   boxOnBluePad ( B/p )   manOnBluePad ( 5 )
// BattleFieldGround ( f )

class GameLevel {

    var level1: Array<String>
    var level2: Array<String>
    var level3: Array<String>

    init {
        level1 = arrayOf(
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "ffffffcffffff",
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "#############",
                "#           #",
                "#           #",
                "#           #",
                "#           #",
                "#   r   .   #",
                "# g $       #",
                "# $ @ $ o   #",
                "#   $       #",
                "#   b       #",
                "#############"
        )

        level2 = arrayOf(
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "ffffffcffffff",
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "fffffffffffff",
                "#############",
                "#           #",
                "#           #",
                "#           #",
                "#           #",
                "#    r  .   #",
                "#     $ $   #",
                "#       @   #",
                "#           #",
                "#           #",
                "#############"
        )

        level3 = arrayOf(
                "             ",
                "             ",
                "             ",
                "             ",
                "      c      ",
                "             ",
                "             ",
                "             ",
                "             ",
                "#############",
                "#    #   g  #",
                "#   #     # #",
                "#     $ g   #",
                "#   ####    #",
                "#    r      #",
                "#    $ #    #",
                "#    @      #",
                "#           #",
                "#           #",
                "#############"
        )
    }


    val levelList = ArrayList<Array<String>>()


    fun loadLevels(): ArrayList<Array<String>> {
        if (levelList.isEmpty()) {
            levelList.add(level1)
            levelList.add(level2)
            levelList.add(level3)
        }
        return levelList
    }


    fun getLevel(levelIndex: Int): Array<String> {
        loadLevels()
        return levelList.get(levelIndex)
    }


    fun getNameList(): ArrayList<String> {
        loadLevels()
        var levelName: ArrayList<String> = ArrayList<String>()

        for (i in levelList.indices) {
            levelName.add("level ${i+1} ")
        }
        return levelName
    }
}




