package com.example.cychen.alicerabbit

import android.content.Intent
import android.media.AudioManager
import android.media.SoundPool
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.WindowManager
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_level_selection.*

/**
 * Created by cychen on 03/01/2018.
 */

class GameLevelSelectionActivity : AppCompatActivity(){

    val thisContext = this@GameLevelSelectionActivity

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        this.window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_level_selection)

        val cellTextViewSource = R.layout.gridview_cell
        val cellElementSource: ArrayList<String> = GameLevel().getNameList()

        val gvAdapter = ArrayAdapter<String>(
                thisContext,
                cellTextViewSource,
                cellElementSource
        )

        gview.adapter = gvAdapter

        gview.setOnItemClickListener{ parent, view, positionIndex, id ->
           toSelectedLevelActivity(positionIndex)
          //  Toast.makeText(thisContext,"onitemok", Toast.LENGTH_SHORT).show()
        }
    }


    fun toSelectedLevelActivity(positionIndex: Int) {
        val intentToSelectedLevel: Intent = Intent(thisContext, SelectedLevelActivity:: class.java)
        intentToSelectedLevel.putExtra("selectedLevelIndex", positionIndex)
        startActivity(intentToSelectedLevel)
    }


}