package com.example.cychen.alicerabbit

import android.graphics.RectF

/**
 * Created by cychen on 27/02/2018.
 */
class MoveMagic {

    companion object {

        private var num = NumSetting(13,20,9,80)
        private var lengthUnit = num.cellWidth.toFloat()

        fun move(magic:Magic, numNew: NumSetting): Magic {
            var magicNew = magic
            num = numNew
            lengthUnit = num.cellWidth.toFloat()
            when(magic.magicType) {
                "wave" -> magicNew = wave(magic)
                "blackHole" -> magicNew = blackHole(magic)
                "eddyWind" -> magicNew = eddyWind(magic)
                "snow" -> magicNew = snow(magic)
            }
            return magicNew
        }

        fun blackHole(magic:Magic): Magic {
            magic.centerXY[0] = magic.centerXYScaled[0] * lengthUnit
            magic.centerXY[1] = magic.centerXYScaled[1] * lengthUnit
            magic.radius = magic.radiusScaled * lengthUnit
            magic.collisionBox = rectF(magic.collisionBoxScaled)
            return magic
        }

        fun wave(magic:Magic): Magic {
            val deltaX = 0.25f
            val deltaY = 0.15f

            val newTopY = magic.imageRectFScaled.top - deltaY
            val newBottomY = magic.imageRectFScaled.bottom + deltaY
            var newLeftX = magic.imageRectFScaled.left
            var newRightX = magic.imageRectFScaled.right

            if (magic.waveDirection == "R") {
                newRightX = magic.imageRectFScaled.right + deltaX
            } else if (magic.waveDirection == "L") {
                newLeftX = magic.imageRectFScaled.left - deltaX
            }
            val boundRect = RectF(1f, 1f,
                            num.numOfCellsPerRow - 1f,
                            num.numOfCellsPerColumnInBattleField - 1f
            )
            val imageRectFScaled = RectF( newLeftX, newTopY, newRightX, newBottomY )
            if (boundRect.contains(imageRectFScaled)) {
                magic.imageRectFScaled = imageRectFScaled
                magic.imageRectF = rectF(magic.imageRectFScaled)
                if ( magic.waveDirection == "R") {
                    magic.collisionBoxScaled = RectF(
                            newRightX - 0.5f,
                            newTopY - 0.5f,
                            newRightX + 0.5f,
                            newBottomY + 0.5f
                    )
                } else {
                    magic.collisionBoxScaled = RectF(
                            newLeftX - 0.5f,
                            newTopY - 0.5f,
                            newLeftX + 0.5f,
                            newBottomY + 0.5f
                    )
                }
                magic.collisionBox = rectF( magic.collisionBoxScaled)
            } else {
                magic.lifeStatus = false
            }
            return magic
        }

        fun snow(magic: Magic): Magic {
            /*
            magic.centerXYScaledList.clear()
            magic.centerXYList.clear()
            magic.radius = magic.radiusScaled * lengthUnit
            for (i in 0..49) {
                val cx = ((Math.random() * (125 - 5)) * 0.1 + 0.5).toFloat()
                val cy = ((Math.random() * (85 - 5)) * 0.1 + 0.5).toFloat()
                magic.centerXYScaledList.add(arrayOf(cx,cy))
                magic.centerXYList.add(arrayOf(cx* lengthUnit,cy* lengthUnit))
            }
            */

            val stepSize = 0.05f
            val newCenterXYScaledList = mutableListOf<Array<Float>>()
            val newCenterXYList = mutableListOf<Array<Float>>()

            for (center in magic.centerXYScaledList) {
                var centerXScaled = center[0]
                var centerYScaled = center[1] + stepSize
                if (centerYScaled > 8.5) {
                    centerXScaled = ((Math.random() * (125 - 5)) * 0.1 + 0.5).toFloat()
                    centerYScaled = ((Math.random() * (65 - 5)) * 0.1 + 0.5).toFloat()
                }
                val centerX = centerXScaled * lengthUnit
                val centerY = centerYScaled * lengthUnit
                newCenterXYScaledList.add(arrayOf(centerXScaled, centerYScaled))
                newCenterXYList.add(arrayOf(centerX, centerY))

                magic.centerXYScaledList = newCenterXYScaledList
                magic.centerXYList = newCenterXYList
                magic.radius = magic.radiusScaled * lengthUnit
            }

            return magic
        }


        fun eddyWind(magic:Magic): Magic {
            val stepSizeScaled = 0.25f
            val imageRectFWidthScaled = 1f
            val collisionBoxWidthScaled = 1.5f * imageRectFWidthScaled
            val dirInt = magic.eddyWindDirection

            when (dirInt) {
                1 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0] +  stepSizeScaled
                    magic.centerXYScaled[1] = magic.centerXYScaled[1]
                }
                2 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0] -  stepSizeScaled
                    magic.centerXYScaled[1] = magic.centerXYScaled[1]
                }
                3 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0]
                    magic.centerXYScaled[1] = magic.centerXYScaled[1] +  stepSizeScaled/1.6f
                }
                4 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0]
                    magic.centerXYScaled[1] = magic.centerXYScaled[1] -  stepSizeScaled/1.6f
                }
                5 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0] + stepSizeScaled
                    magic.centerXYScaled[1] = magic.centerXYScaled[1] + stepSizeScaled/1.6f
                }
                6 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0] - stepSizeScaled
                    magic.centerXYScaled[1] = magic.centerXYScaled[1] + stepSizeScaled/1.6f
                }
                7 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0] +  stepSizeScaled
                    magic.centerXYScaled[1] = magic.centerXYScaled[1] - stepSizeScaled/1.6f
                }
                8 -> {
                    magic.centerXYScaled[0] = magic.centerXYScaled[0] -  stepSizeScaled
                    magic.centerXYScaled[1] = magic.centerXYScaled[1] - stepSizeScaled/1.6f
                }
            }

            if ( windIsBound(magic.centerXYScaled) ) {
                magic.imageRectFScaled = rectFScaled(magic.centerXYScaled,imageRectFWidthScaled )
                magic.imageRectF = rectF(magic.imageRectFScaled)
                magic.collisionBoxScaled = rectFScaled(magic.centerXYScaled,collisionBoxWidthScaled)
                magic.collisionBox = rectF(magic.collisionBoxScaled)
            } else {
                magic.lifeStatus = false
            }
            return magic
        }

        fun windIsBound(centerXYScaled:Array<Float>): Boolean {
            val maxX = (num.numOfCellsPerRow - 1.0f)
            val maxY = (num.numOfCellsPerColumnInBattleField - 1.0f)
            val min = 1.0f
            return (centerXYScaled[0] in min..maxX)&&(centerXYScaled[1] in min..maxY)
        }

        fun rectFScaled(centerXYScaled:Array<Float>,rectFWidthScaled:Float) : RectF {
            return RectF(
                centerXYScaled[0] - 0.5f * rectFWidthScaled,
                centerXYScaled[1] - 0.5f * rectFWidthScaled,
                centerXYScaled[0] + 0.5f * rectFWidthScaled,
                centerXYScaled[1] + 0.5f * rectFWidthScaled
            )
        }

        fun rectF(rectFScaled:RectF): RectF {
            return RectF(
                rectFScaled.left * lengthUnit,
                rectFScaled.top * lengthUnit,
                rectFScaled.right * lengthUnit,
                rectFScaled.bottom * lengthUnit
            )
        }

    }
}
