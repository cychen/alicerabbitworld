package com.example.cychen.alicerabbit

import android.util.Log

/**
 * Created by cychen on 30/01/2018.
 */
class InvaderCollection {

    companion object {
        var invaderID = 1
        var invaderList = mutableListOf<Invader>()

        fun addInvader(newInvader: Invader){
            newInvader.ID = invaderID
            invaderList.add(newInvader)
            invaderID += 1
        }

        fun updateInvaderList(newInvaderList: MutableList<Invader>){
            this.invaderList = newInvaderList
        }

    }
}