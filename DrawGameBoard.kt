package com.example.cychen.alicerabbit

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.support.v4.content.ContextCompat
import android.util.Log

/**
 * Created by cychen on 12/01/2018.
 */
class DrawGameBoard(private val ctx: Context, private val canvas: Canvas, private val num: NumSetting) {

    private val res: Resources = ctx.resources
    var stringToBitmapMap: MutableMap<String, Bitmap>
    var stringToRectMap: MutableMap<String, Rect>
    private val newSelectedLevel: Array<String> = MoveResult.selectedLevel

    init {
        stringToRectMap = GameBitmap(res).srcStringRectMap()
        stringToBitmapMap = GameBitmap(res).srcStringBitmapMap()
    }

    fun drawBoard() {

        for (rIndex in 0 until newSelectedLevel.size) {
            for ((cIndex,value) in newSelectedLevel[rIndex].withIndex()) {

                val destRect = rect(cIndex, rIndex)

                if (value.toString() == "@") {
                    canvas.drawBitmap(stringToBitmapMap[" "], stringToRectMap[" "], destRect, null)
                    canvas.drawBitmap(stringToBitmapMap[value.toString()], stringToRectMap[value.toString()], destRect, null)

                } else if (value == 'c') {
                    canvas.drawBitmap(stringToBitmapMap["f"], stringToRectMap["f"], destRect, null)
                    canvas.drawBitmap(stringToBitmapMap[value.toString()], stringToRectMap[value.toString()], destRect, null)
                }
                canvas.drawBitmap(stringToBitmapMap[value.toString()], stringToRectMap[value.toString()], destRect, null)
            }
        }
    }


    fun drawBackGround(){
        val BGColor: Int = ContextCompat.getColor(ctx, R.color.gameviewBaseColor)
        val BGBrush = Paint()
        BGBrush.color = BGColor
        canvas.drawRect(0f,0f, canvas.width.toFloat(), canvas.height.toFloat(),BGBrush)
    }


    fun drawGridLine() {
        val gridLineColor = ContextCompat.getColor(ctx,R.color.gridLineColor)
        val gridLinePain = Paint()
        gridLinePain.color = gridLineColor
        val (numOfCellsPerRow, numOfCellsPerColumn, _, cellWidth) = num
        for (cIndex in 0..numOfCellsPerRow){
            for ( rIndex in 0..numOfCellsPerColumn){
                var yIndex = (rIndex*cellWidth).toFloat()
                var xIndex = (cIndex*cellWidth).toFloat()
                val xEndIndex = (numOfCellsPerRow*cellWidth).toFloat()
                val yEndIndex = (numOfCellsPerColumn*cellWidth).toFloat()

                canvas.drawLine(0f, yIndex, xEndIndex, yIndex, gridLinePain)
                canvas.drawLine(xIndex,0f, xIndex, yEndIndex, gridLinePain)
            }
        }
    }


   private fun rect(cIndex: Int, rIndex: Int): Rect {
        val (_,_,_,cellWidth)= num
        val leftX = cIndex * cellWidth
        val topY = rIndex * cellWidth
        val rightX = leftX + cellWidth
        val bottomY = topY +  cellWidth
        return Rect(leftX,topY,rightX,bottomY)
    }



}