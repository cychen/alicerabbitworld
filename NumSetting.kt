package com.example.cychen.alicerabbit

import android.provider.MediaStore

/**
 * Created by cychen on 15/01/2018.
 */
data class NumSetting(
        val numOfCellsPerRow: Int = 13,
        val numOfCellsPerColumn: Int = 20,
        val numOfCellsPerColumnInBattleField: Int = 9,
        var cellWidth: Int)