package com.example.cychen.alicerabbit

import android.graphics.Rect
import android.graphics.RectF
import android.util.Log

/**
 * Created by cychen on 23/01/2018.
 */
class MoveInvader {

	companion object {
		val caveBox = CaveBoundary.caveBox()
		private const val imageRectFWidthScaled = 1f
		var lengthUnit = 1f

		fun move(invaderInit:Invader,num:NumSetting): Invader {
			var invader = invaderInit
			val magicList = MagicCollection.magicList
			lengthUnit = num.cellWidth.toFloat()

			var invaderNewX = invader.centerXYScaled[0] + invader.stepSizeScaled[0]
			var invaderNewY = invader.centerXYScaled[1] + invader.stepSizeScaled[1]

			invader = checkAndDoInvaderAndCaveCollision(invader,invaderNewX,invaderNewY)
			if (!invader.lifeStatus){ return invader }

			magicList.forEach {
				if (it.magicType == "snow") {
					invader = AEMagic(invader,it)
					/*
					invader.stepSizeScaled[0] *= 0.9f
					invader.stepSizeScaled[1] *= 0.9f
					invader.lifeRemained = invader.lifeRemained - it.attackDamagePoint
					Log.d("gameview","${invader.lifeRemained}")
					if (invader.lifeRemained <= 0){
						invader.lifeStatus = false
						return invader
					}
					*/
				}
				else {
					if (it.collisionBoxScaled.contains(invaderNewX, invaderNewY)) {
						if (invader.lifeRemained > it.attackDamagePoint) {
							invader.lifeRemained = invader.lifeRemained - it.attackDamagePoint
							invader = InvaderAndMagicCollision.doCollision(invader, it, num)
						} else {
							invader.lifeStatus = false
						}
						if (!invader.lifeStatus) {
							return invader
						}
					}
				}
			}
			return invader
		}

		fun AEMagic(invaderInit: Invader,magic: Magic): Invader {
			var invader = invaderInit
			invader.stepSizeScaled[0] *= 0.9f
			invader.stepSizeScaled[1] *= 0.9f
			invader.lifeRemained = invader.lifeRemained - magic.attackDamagePoint
			if (invader.lifeRemained <= 0){
				invader.lifeStatus = false
			}
			return invader
		}

		fun singleMagic(invaderInit: Invader,magic: Magic): Invader {
			var invader = invaderInit
			return invader
		}
		fun checkAndDoInvaderAndCaveCollision(invaderInit: Invader,invaderNewX: Float,invaderNewY: Float): Invader {
			var invader = invaderInit
			invader = when (caveBox.contains(invaderNewX, invaderNewY)) {
				true -> doInvaderAttackCave(invader)
				else -> doUpdateInvaderPosition(invader, invaderNewX, invaderNewY)
			}
			return invader
		}

		fun doInvaderAttackCave(invaderInit:Invader):Invader {
			var invader = invaderInit
			val caveAttackDamagePoint = 3
			if ( invader.lifeRemained > caveAttackDamagePoint ){
				invader.lifeRemained = invader.lifeRemained - caveAttackDamagePoint
			} else {
				invader.lifeStatus = false
			}
			return invader
		}

		fun doUpdateInvaderPosition(invaderInit:Invader,invaderNewX: Float,invaderNewY: Float): Invader {
			var invader = invaderInit
			invader.centerXYScaled = arrayOf(invaderNewX, invaderNewY)
			invader.imageRectFScaled = rectFScaled(invaderNewX, invaderNewY)
			invader.imageRectF = rectF(invader.imageRectFScaled)
			invader.collisionBoxScaled = invader.imageRectFScaled
			invader.collisionBox = invader.imageRectF
			return invader
		}

		fun rectFScaled(invaderX:Float, invaderY:Float): RectF {
			return RectF(invaderX - 0.5f * imageRectFWidthScaled,
						 invaderY - 0.5f * imageRectFWidthScaled,
						 invaderX +  0.5f * imageRectFWidthScaled,
						 invaderY +  0.5f * imageRectFWidthScaled )
		}

		fun rectF(rectF:RectF): RectF {
			return RectF(rectF.left * lengthUnit,
						 rectF.top * lengthUnit,
						 rectF.right * lengthUnit,
						 rectF.bottom * lengthUnit )
		}


	}
}




