package com.example.cychen.alicerabbit

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        this.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)

        setContentView(R.layout.activity_main)

        fun toastText() {
            Toast.makeText(this@MainActivity,"hello33", Toast.LENGTH_SHORT).show()
        }

        fun startGameLevelSelectionActivity(){
            val intentGameLevelSelection = Intent(this@MainActivity, GameLevelSelectionActivity::class.java)
            startActivity(intentGameLevelSelection)
        }

        game_start.setOnClickListener { startGameLevelSelectionActivity() }

    }




}
