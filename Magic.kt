package com.example.cychen.alicerabbit

import android.graphics.Rect
import android.graphics.RectF
import android.util.Log

/**
 * Created by cychen on 12/02/2018.
 */
class Magic {
    var ID: Int = 1
    var magicType: String = "magic"
    var friend: Boolean = true
    var life: Int = 3
    var lifeRemained: Int =3
    var attackDamagePoint: Int = 1
    var lifeStatus: Boolean = true

    var waveDirection: String = "R"
    var waveStartAngle: Float = 0f
    var waveSweepAngle: Float = 60f

    var collisionBoxScaled: RectF = RectF(4f,4f,5f,5f)
    var collisionBox: RectF = collisionBoxScaled

    var centerXYScaled: Array<Float> = arrayOf(6f,6f)
    var centerXY : Array<Float> = centerXYScaled
    var radiusScaled: Float = 1f
    var radius: Float = radiusScaled

    var eddyWindDirection: Int = 1
    var imageRectFScaled: RectF = RectF(4f,4f,5f,5f)
    var imageRectF: RectF = imageRectFScaled

    var centerXYScaledList = mutableListOf<Array<Float>>()
    var centerXYList = mutableListOf<Array<Float>>()

   // var snowRadiusScaled = 0.1f
  //  var snowRadiusF = snowRadiusScaled

    companion object {
        var imageRectFWidthScaled = 1f
        var collisionBoxWidthScaled = 1f

        fun wave (direction: String,cavePosition: Array<Int>) {
            val wave = Magic()
            wave.magicType = "wave"
            wave.waveDirection = direction
            if (direction == "R") {
                wave.waveStartAngle = 300f
                wave.waveSweepAngle = 120f
            } else if (direction == "L"){
                wave.waveStartAngle = 120f
                wave.waveSweepAngle = 120f
            }
            val centerX = cavePosition[0] + 0.5f * imageRectFWidthScaled
            val centerY = cavePosition[1] + 0.5f * imageRectFWidthScaled
            wave.imageRectFScaled = rectFScaled(centerX,centerY, imageRectFWidthScaled)
            wave.imageRectF = wave.imageRectFScaled
            wave.collisionBoxScaled = wave.imageRectFScaled
            wave.collisionBox = wave.imageRectF
            MagicCollection.addMagic(wave)
        }

        fun blackHole(){
            //boundary: min = blackHoleRadiusF + 0.75f
            // xMax = num.numOfCellsPerRow - min
            // yMax = num.numOfCellsPerColumnInBattleField - min
            // (centerX  in min..xMax) && ( centerY in min..yMax)
            val blackHole = Magic()
                blackHole.magicType = "blackHole"
                blackHole.centerXYScaled = arrayOf(9.5f,4.5f)
                blackHole.radiusScaled = 0.6f
                blackHole.radius = 0.6f
            collisionBoxWidthScaled = 2f * blackHole.radiusScaled

            blackHole.collisionBoxScaled = rectFScaled(
                    blackHole.centerXYScaled[0],blackHole.centerXYScaled[1], collisionBoxWidthScaled)

            blackHole.collisionBox = blackHole.collisionBoxScaled
            MagicCollection.addMagic(blackHole)
        }

        fun eddyWind(direction:String,cavePosition: Array<Int>){
            val eddyWind = Magic()
            eddyWind.magicType ="eddyWind"
            collisionBoxWidthScaled = 2f * imageRectFWidthScaled

            eddyWind.eddyWindDirection = eddyWindMap(direction)
            val centerX = cavePosition[0] + 0.5f * imageRectFWidthScaled
            val centerY = cavePosition[1] + 0.5f * imageRectFWidthScaled

            eddyWind.centerXYScaled = arrayOf(centerX,centerY)
            eddyWind.imageRectFScaled = rectFScaled(centerX,centerY,imageRectFWidthScaled)
            eddyWind.imageRectF = eddyWind.imageRectFScaled
            eddyWind.collisionBoxScaled = rectFScaled(centerX,centerY, collisionBoxWidthScaled)
            eddyWind.collisionBox = eddyWind.collisionBoxScaled
            MagicCollection.addMagic(eddyWind)
        }

        fun eddyWindMap(direction:String): Int {
            var dirInt = 1
            when (direction) {
                "R" -> dirInt = 1
                "L" -> dirInt = 2
                "D" -> dirInt = 3
                "U" -> dirInt = 4
                "DR" -> dirInt = 5
                "DL" -> dirInt = 6
                "UR" -> dirInt = 7
                "UL" -> dirInt = 8
            }
            return dirInt
        }

        fun snow() {
            val snow = Magic()
            snow.magicType = "snow"
            snow.centerXYScaledList.clear()
            snow.centerXYList.clear()
            snow.radiusScaled = 0.1f
            for (i in 0..49) {
                val cx = ((Math.random() * (125 - 5)) * 0.1 + 0.5).toFloat()
                val cy = ((Math.random() * (65 - 5)) * 0.1 + 0.5).toFloat()
                snow.centerXYScaledList.add(arrayOf(cx,cy))
            }
            MagicCollection.addMagic(snow)
        }

        fun rectFScaled(centerX:Float, centerY:Float, widthScaled:Float): RectF {
            return RectF(centerX - 0.5f * widthScaled,
                    centerY - 0.5f * widthScaled,
                    centerX +  0.5f * widthScaled,
                    centerY +  0.5f * widthScaled)
        }

    }
}