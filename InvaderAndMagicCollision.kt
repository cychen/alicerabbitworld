package com.example.cychen.alicerabbit

import android.graphics.RectF
import android.util.Log

/**
 * Created by cychen on 28/02/2018.
 */
class InvaderAndMagicCollision {

    companion object {
        var lengthUnit = 1f
        var imageRectFWidthScaled = 1f
        var num = NumSetting(13,20,9,80)

        fun doCollision(invaderInit:Invader,magic:Magic,numNew:NumSetting): Invader {
            num = numNew
            lengthUnit = num.cellWidth.toFloat()
            var invader = invaderInit

           invader = when (magic.magicType){
                "blackHole" -> {  doInvaderBlackHoleCollision(invader) }
                "wave" -> { doWaveMove(invader) }
                "eddyWind" -> { doWindMove(invader,magic) }
              //  "snow" -> { doInvaderSnowCollision(invader) }
               else -> invader
            }
            return invader
        }

        fun doInvaderBlackHoleCollision(invader: Invader): Invader {
          //  imageRectFWidthScaled = invader.lifeRemained * 0.1f
            imageRectFWidthScaled *=  0.90f
            invader.imageRectFScaled = rectFScaled(invader.centerXYScaled[0],invader.centerXYScaled[1])
            invader.imageRectF = rectF(invader.imageRectFScaled)
            invader.collisionBoxScaled = invader.imageRectFScaled
            invader.collisionBox = invader.imageRectF
            return invader
        }

        fun doWindMove(invader: Invader,magic: Magic): Invader {
            val min = 0.75f
            val ymax = num.numOfCellsPerColumnInBattleField - 0.75f
            val xmax = num.numOfCellsPerRow - 0.75f
            imageRectFWidthScaled *=  0.98f
            invader.centerXYScaled = magic.centerXYScaled
            if ( (invader.centerXYScaled[0] in min..xmax) && (invader.centerXYScaled[1] in min..ymax)){
                updateInvaderRect(invader)
            } else {
                invader.lifeStatus = false
            }
            return invader
        }

        fun doWaveMove(invader: Invader): Invader {
            val min = 0.75f
            val xmax = num.numOfCellsPerRow - 0.75f
            val ymax = num.numOfCellsPerColumnInBattleField - 0.75f

            val backXStepSizeScaled = invader.stepSizeScaled[0] * 5f
            val backYStepSizeScaled = invader.stepSizeScaled[1] * 5f

            val newCenterX = invader.centerXYScaled[0] - backXStepSizeScaled
            val newCenterY = invader.centerXYScaled[1] - backYStepSizeScaled

            if ( (newCenterX in min..xmax) && (newCenterY in min..ymax) ) {
                invader.centerXYScaled = arrayOf(newCenterX,newCenterY)
                updateInvaderRect(invader)
            } else {
                invader.lifeStatus = false
            }
            return invader
        }



        fun doInvaderSnowCollision(invader: Invader): Invader{
            invader.stepSizeScaled[0] *= 0.9f
            invader.stepSizeScaled[1] *= 0.9f
            return invader
        }

        fun updateInvaderRect(invader:Invader): Invader {
            invader.imageRectFScaled = rectFScaled(invader.centerXYScaled[0],invader.centerXYScaled[1])
            invader.imageRectF = rectF(invader.imageRectFScaled)
            invader.collisionBoxScaled = invader.imageRectFScaled
            invader.collisionBox = invader.imageRectF
            return invader
        }

        fun rectFScaled(centerXScaled:Float, centerYScaled:Float): RectF {
            return RectF(centerXScaled - 0.5f * imageRectFWidthScaled,
                        centerYScaled - 0.5f * imageRectFWidthScaled,
                        centerXScaled +  0.5f * imageRectFWidthScaled,
                        centerYScaled +  0.5f * imageRectFWidthScaled)
        }

        fun rectF(rectF:RectF): RectF {
            return RectF(rectF.left * lengthUnit,
                        rectF.top * lengthUnit,
                        rectF.right * lengthUnit,
                        rectF.bottom * lengthUnit )
        }

    }
}